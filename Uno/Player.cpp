#include "pch.h"
#include "Player.h"
#include "Carta.h"
#include <vector>
#include <iostream>
#include "Game.h"
#include "Prob.h"

extern const int tamanhoDoDeck;

Player::Player()
	: Mao(),
	m_isHuman(false),
	m_simbolo(" "),
	m_simbolo2(" "),
	m_name {""}
{
	
}


Player::~Player()
{
}

void Player::SetCarta(Carta cart, int index) {
	Mao[STVC(index)] = cart;
}
void Player::PrintMao() {
	int count{ 1 };
	for (Carta &e : Mao) {
		std::cout << (count<10?"   0": "   ")<< count <<"-";
		e.PrintCarta();
		count++;
	}
}
void Player::TirarCartaDaMao(int index) {
	Mao[STVC(index)] = Mao[Mao.size() - STVC(1)];
	Mao.resize(Mao.size() - STVC(1));
}
void Player::SetMaoSize(int &numeroDeCartas) {
	m_numeroDeCartas = numeroDeCartas;
	Mao.resize(STVC(m_numeroDeCartas));
}
std::string Player::GetName() { return m_name; }
void Player::SetName(std::string name) {
	m_name = name;
}
bool Player::PossoJogar(Carta &ultima, Carta &esta, Carta::Cor cor, int &contParaComprar) {
	//Se a carta que se quer jogar for do preta, pode jogar desde que a carta na mesa n�o seja +2 ou +4
	if (esta.GetTipo() == Carta::Tipo::TIPO_ACAO && ultima.GetNumero()!=Carta::Numero::N_COMPRAR2 && ultima.GetNumero() != Carta::Numero::N_PRETA_COMPRAR4)
		return true;
	//Se carta na mesa for  +2
	else if (ultima.GetNumero() == Carta::Numero::N_COMPRAR2) {
		//Se a carta ainda n�o teve seu efeito, pode jogar ou +2 ou +4
		if (contParaComprar > 1) {
			if ((static_cast<int>(ultima.GetNumero()) == static_cast<int>(esta.GetNumero()))
				|| esta.GetNumero() == Carta::Numero::N_PRETA_COMPRAR4) {
				return true;
			}
		}
		//Se a carta teve seu efeito, pode jogar uma preta ou uma com a mesma cor
		else if (((ultima.GetCor() == esta.GetCor() || esta.GetCor() == Carta::Cor::COR_PRETA || esta.GetNumero() ==Carta::Numero::N_COMPRAR2) && contParaComprar == 1))
			return true;

	}
	//Se a carta na mesa for +4
	else if (ultima.GetNumero() == Carta::Numero::N_PRETA_COMPRAR4) {
		//Se a carta n�o teve seu efeito ainda, pode jogar uma +4 ou uma +2 da cor escolhida por quem jogou a +4 
		if (contParaComprar > 1) {
			if (((cor == esta.GetCor() && esta.GetNumero() == Carta::Numero::N_COMPRAR2)) || esta.GetNumero() == Carta::Numero::N_PRETA_COMPRAR4)
				return true;
		}
		//Se a carta teve seu efeito, pode jogar uma preta ou uma com a cor escolhida por quem jogou a +4
		else if (((cor == esta.GetCor() && contParaComprar==1) || (esta.GetCor() == Carta::Cor::COR_PRETA && contParaComprar == 1)))
			return true;
		
	}

	else if ((cor == esta.GetCor()) || (ultima.GetNumero() == esta.GetNumero()))
		return true;
	else if (cor != esta.GetCor()  && ultima.GetNumero() != esta.GetNumero()) {
		if (GetHuman()) {
			std::cout << "\n   " << GetName() <<
				", sua escolha n�o tem o mesmo numero/simbolo ou cor que a carta que esta na mesa.\n   A carta na mesa e: ";
			if (ultima.GetCor() == Carta::Cor::COR_PRETA) {
				ultima.PrintCarta();
				std::cout << ". A cor escolhida foi: ";
					if (cor == Carta::Cor::COR_AZUL)
						std::cout << "Azul.";
					else if (cor == Carta::Cor::COR_VERMELHO)
						std::cout << "Vermelho.";
					else if (cor == Carta::Cor::COR_VERDE)
						std::cout << "Verde.";
					else if (cor == Carta::Cor::COR_AMARELO)
						std::cout << "Amarelo.";
			}
			else 
				ultima.PrintCarta();
			
			std::cout << "\n";
		}
		return false;
	}
	return false;
}

void Player::ComprarCarta(Carta *&carta, int count){

	Mao.resize(Mao.size() + count);
	for (int i{ 0 }; i <count; i++) {
		Mao[STVC(Mao.size() - (i+1))] = *carta;
		assert((*carta).GetTipo()!=Carta::Tipo::TIPO_NULO && 
		"\n   Jogador tirou carta NULA do deck ");
		carta++;
	}
}

//Fun��o roda a jogada de humanos e chama a fun��o AI para rodar a jogada dos NPCs
Carta Player::Jogar(std::vector<Player> &Jogadores, int &ProximoJog, Carta &ultima, Carta::Cor Cor, bool &jogou, bool &comprou, Carta *proxima, int &contParaComprar, bool &BGBotGame) {

	if (GetHuman()) {

		if (ultima.GetCor() == Carta::Cor::COR_PRETA) {
			std::cout << "   A cor escolhida foi ";
			if (Cor == Carta::Cor::COR_AZUL)
				std::cout << "Azul.\n";
			else if (Cor == Carta::Cor::COR_VERMELHO)
				std::cout << "Vermelho.\n";
			else if (Cor  == Carta::Cor::COR_VERDE)
				std::cout << "Verde.\n";
			else if (Cor  == Carta::Cor::COR_AMARELO)
				std::cout << "Amarelo.\n";
		}

		if (contParaComprar > 1)
			std::cout << "   Cartas acumuladas para comprar: " << contParaComprar << ". \n";
		else if (contParaComprar == 1 && ((ultima.GetNumero() == Carta::Numero::N_COMPRAR2) || (ultima.GetNumero() == Carta::Numero::N_PRETA_COMPRAR4)))
			std::cout << "   Cartas acumuladas para comprar: 0. Carta ja teve seu efeito.\n\n";

		OrgMaoD();
		PrintMao();
			
		std::cout << "\n   "<< GetName() 
					<<", escolha uma carta para jogar, digite o index dela, ou 0 para comprar: \n ";

		int index;
		bool jaComprou{ false };

		//Processo de escolha da carta para jogar e de compra, 
		//se uma carta � jogada o loop acaba, do contr�rio 
		//a fun��o retorna de dentro do loop
		
		while (true) {
			DigiteZero(index, 0, GetMaoSize(), "\n   Escolha uma carta valida \n");
			index = (index - 1);
			if (index == -1 && !comprou) {
				
				ComprarCarta(proxima,contParaComprar);
				comprou = true;
				
				if (contParaComprar > 1) {
					jogou = false;
					std::cout << "\n   Voce comprou "<< contParaComprar << ((contParaComprar >1)?(" cartas. "):(" carta. ")) << "Digite zero para passar a vez.\n";
					PrintMao();
					DigiteZero(index, 0, 0, "\n Voce nao pode mais jogar nesta rodada. Digite zero para passar a vez.\n");
					return Carta();
				}
				else {
					std::cout << "\n   Voce comprou uma carta, ela aparece por ultimo abaixo. Jogue ela ou digite zero para passar a vez.\n";
					PrintMao();
					continue;
				}
			}
			else if (index == -1 && comprou) {
				jogou = false;
				return Carta();
			}
			else if (PossoJogar(ultima, Mao[STVC(index)], Cor, contParaComprar))
				break;
			else
				std::cout <<"\n   " <<GetName()
				<< ",  escolha outra carta:\n";
		}
		Carta temp = Mao[STVC(index)];
		TirarCartaDaMao(index);
		return temp;
	}
	//Se n�o for humano a AI decide o que fazer.
	else if(!GetHuman()) {
		return AI(Jogadores,ProximoJog,ultima, Cor, jogou, comprou, proxima, contParaComprar, BGBotGame);
	}
	
}
Carta::Cor Player::EscolherCor(Carta &ultima, Carta &Penultima, Carta::Cor &aCor, bool &escolheu, bool &jogou) {
	
	//Se a ultima carta � preta e ainda nao se escolheu
	//Se a ultima E a penultima carta sao pretas, e j� se escolheu para a penultima
	// Se aCor � preta quer dizer que � o primeiro descarte do jogo
	/*assert(!(&ultima) && "Ao EscolherCor(), &ultima carta � um nullptr.");
	assert(!(&Penultima) && "Ao EscolherCor(), &penultima carta � um nullptr.");*/

	if ((((ultima).GetCor() == Carta::Cor::COR_PRETA) && !escolheu && jogou)||
		(((ultima).GetCor() == Carta::Cor::COR_PRETA )&& escolheu && ((Penultima).GetCor() == Carta::Cor::COR_PRETA && jogou))
			|| (((ultima).GetCor() == Carta::Cor::COR_PRETA) && !escolheu && ((Penultima).GetCor() == Carta::Cor::COR_PRETA && !jogou))) {
			
		int color;
		if (GetHuman()) {
			std::cout << "\n   " << GetName()
				<< ", digite zero para escolher uma cor: \n";
			int zero;
			DigiteZero(zero, 0, 0, "\n   Digite zero para escolher uma cor: \n");

			std::cout << "\n   Suas cartas: \n";
			PrintMao();
				
				std::cout << "\n   " <<GetName()
					<< ", a carta na mesa e um"<< (((ultima).GetNumero() == Carta::Numero::N_PRETA_COMPRAR4)?" mais quatro. ": " coringa. ")
				
				<<"Escolha uma cor: \n   1 - AZUL \n   2 - VERMELHO \n   3 - VERDE \n   4 - AMARELO\n";
			
				DigiteZero(color, 1, 4, "\n   Escolha uma cor valida.\n");
				color = (color - 1);
				escolheu = true;
			return static_cast<Carta::Cor>(color);
		}
		else {
			escolheu = true;
			std::mt19937 Cor(static_cast<unsigned int>(std::time(nullptr)));
			std::uniform_int_distribution<> Escolher(0, 3);
			if (GetMaoSize() == 0) {
				Escolher(Cor);
				return static_cast<Carta::Cor>(Escolher(Cor));
			}
			else {

				if(Mao[STVC(0)].GetCor()!=Carta::Cor::COR_PRETA)
					return Mao[STVC(0)].GetCor();
				else {		
					Escolher(Cor);
					return static_cast<Carta::Cor>(Escolher(Cor));
				}

			}
		}
	}
	else if ((ultima).GetCor() == Carta::Cor::COR_PRETA && escolheu) {
		return aCor;
	}

	else {
		escolheu = false;
		return (ultima).GetCor();
	}
}

void Player::SetHuman(bool Human) { m_isHuman = Human;}//Define se o player e humano ou nao
bool Player::GetHuman() { return m_isHuman; } //Retorna se o player � ou nao humano
std::string Player::GetSimbolo() { return m_simbolo; } //retorna o simbolo (da esquerda) do player que o difere dos outros
std::string Player::GetSimbolo2() { return m_simbolo2; } //retorna o simbolo (da direita)do player que o difere dos outros

//Retorna a quantidade de cartas na m�o do jogador
int Player::GetMaoSize() {
	return static_cast<int>(Mao.size());
}
void Player::SetSimbolo(std::string a, std::string b) {
	m_simbolo = a;
	m_simbolo2 = b;
}

std::vector<Carta>::size_type  Player::STVC(int a) {
	return static_cast<std::vector<Carta>::size_type>(a);
}

std::vector<Player>::size_type Player::STVP(int a) {
	return static_cast<std::vector<Player>::size_type>(a);
}


//Retorna o size_type do aray multidi que guarda a ordem de preval�ncia das cores
std::array<std::array<int, 2>, 4>::size_type Player::STAC(int a){
	return static_cast<std::array<std::array<int, 2>, 4>::size_type>(a);
};

//Retorna o size_type do aray multidi que guarda a ordem de preval�ncia dos numeros
std::array<std::array<int, 2>, 15>::size_type Player::STAN(int a) {
	return static_cast<std::array<std::array<int, 2>, 15>::size_type>(a);
}

void Player::OrgMaoD() {
	//Var primeira tera a cor que mais aparece na mao do player e assim por diante
	const int nDeCores{4};
	
	//D� as cores para as var acima
	//Index 0 fica o numero de cartas
	//Index 1 fica a cor correspondente
	std::array<std::array<int, 2>, nDeCores> nCores;
	nCores[STAC(0)][STAC(0)] = 0;
	nCores[STAC(0)][STAC(1)] = 0;
	nCores[STAC(1)][STAC(0)] = 0;
	nCores[STAC(1)][STAC(1)] = 1;
	nCores[STAC(2)][STAC(0)] = 0;
	nCores[STAC(2)][STAC(1)] = 2;
	nCores[STAC(3)][STAC(0)] = 0;
	nCores[STAC(3)][STAC(1)] = 3;


	for (int i{ 0 }; i < GetMaoSize(); i++) {
		if (Mao[STVC(i)].GetCor() == Carta::Cor::COR_AZUL)
			nCores[STAC(0)][STAC(0)]++;
		else if (Mao[STVC(i)].GetCor() == Carta::Cor::COR_VERMELHO)
			nCores[STAC(1)][STAC(0)]++;
		else if (Mao[STVC(i)].GetCor() == Carta::Cor::COR_VERDE)
			nCores[STAC(2)][STAC(0)]++;
		else if (Mao[STVC(i)].GetCor() == Carta::Cor::COR_AMARELO)
			nCores[STAC(3)][STAC(0)]++;
	}


		//Coloca as cores em descendente
	for (int i{ 0 }; i < nDeCores; i++) {
		int indexMaior{ i };
		for (int j{ i }; j < nDeCores; j++) {
			if (nCores[STAC(j)][STAC(0)] > nCores[STAC(indexMaior)][STAC(0)])
				indexMaior = j;
		}
		SwapInt(nCores[STAC(indexMaior)][STAC(0)], nCores[STAC(i)][STAC(0)]);
		SwapInt(nCores[STAC(indexMaior)][STAC(1)], nCores[STAC(i)][STAC(1)]);
	}


	const int nDeNumero{ 15 };
		
		//Index 0 fica o numero de cartas
		//Index 1 fica numero/simbolo correspondente
		std::array<std::array<int, 2>, nDeNumero> nNumeros;
		
		for (int c{ 0 }, i{ 0 }; i < nDeNumero; i++) {
			for (int j{ 0 }; j < 2; j++) {
				if(j==0)
					nNumeros[STAN(i)][STAN(j)] = 0;
				else {
					nNumeros[STAN(i)][STAN(j)] = c;
					c++;
				}

			}
		}

		//Determina a preval�ncia de cada numero na mao
		for (int i{ 0 }; i < GetMaoSize(); i++) {
			if (Mao[STVC(i)].GetNumero() == Carta::Numero::N_0)
				nNumeros[STAN(0)][STAN(0)]++;
			else if (Mao[STVC(i)].GetNumero() == Carta::Numero::N_1)
				nNumeros[STAN(1)][STAN(0)]++;
			else if (Mao[STVC(i)].GetNumero() == Carta::Numero::N_2)
				nNumeros[STAN(2)][STAN(0)]++;
			else if (Mao[STVC(i)].GetNumero() == Carta::Numero::N_3)
				nNumeros[STAN(3)][STAN(0)]++;
			else if (Mao[STVC(i)].GetNumero() == Carta::Numero::N_4)
				nNumeros[STAN(4)][STAN(0)]++;
			else if (Mao[STVC(i)].GetNumero() == Carta::Numero::N_5)
				nNumeros[STAN(5)][STAN(0)]++;
			else if (Mao[STVC(i)].GetNumero() == Carta::Numero::N_6)
				nNumeros[STAN(6)][STAN(0)]++;
			else if (Mao[STVC(i)].GetNumero() == Carta::Numero::N_7)
				nNumeros[STAN(7)][STAN(0)]++;
			else if (Mao[STVC(i)].GetNumero() == Carta::Numero::N_8)
				nNumeros[STAN(8)][STAN(0)]++;
			else if (Mao[STVC(i)].GetNumero() == Carta::Numero::N_9)
				nNumeros[STAN(9)][STAN(0)]++;
			else if (Mao[STVC(i)].GetNumero() == Carta::Numero::N_PULAR)
				nNumeros[STAN(10)][STAN(0)]++;
			else if (Mao[STVC(i)].GetNumero() == Carta::Numero::N_INVERTER)
				nNumeros[STAN(11)][STAN(0)]++;
			else if (Mao[STVC(i)].GetNumero() == Carta::Numero::N_COMPRAR2)
				nNumeros[STAN(12)][STAN(0)]++;
			else if (Mao[STVC(i)].GetNumero() == Carta::Numero::N_PRETA_COMPRAR4)
				nNumeros[STAN(13)][STAN(0)]++;
			else if (Mao[STVC(i)].GetNumero() == Carta::Numero::N_PRETA_ESCOLHER_COR)
				nNumeros[STAN(14)][STAN(0)]++;
		}

		//Coloca os numeros em descendente de acordo com sua prevalencia
		for (int i{ 0 }; i < nDeNumero; i++) {
			int indexMaior{ i };
			for (int j{ i }; j < nDeNumero; j++) {
				if (nNumeros[STAN(j)][STAN(0)] > nNumeros[STAN(indexMaior)][STAN(0)])
					indexMaior = j;
			}
			SwapInt(nNumeros[STAN(indexMaior)][STAN(0)], nNumeros[STAN(i)][STAN(0)]);
			SwapInt(nNumeros[STAN(indexMaior)][STAN(1)], nNumeros[STAN(i)][STAN(1)]);
		}
	
	//Organiza as cartas de acordo com o tipo delas: numericas, acao colorida e acao preta.
		int vezes{ 4 };
	for (int a{ 0 }; a < vezes; a++) {
		for (int i{ 0 }; i < GetMaoSize() - 1; i++) {

			bool swap{ false };

			for (int j{ i }; j < GetMaoSize() - 1; j++) {

				//Primeiro nivel: organiza pelo tipo 
				//Se a carta da esquerda for +4, sempre troca com a da direita
				if (Mao[STVC(j)].GetNumero() == Carta::Numero::N_PRETA_COMPRAR4 && Mao[STVC(j + 1)].GetNumero() != Carta::Numero::N_PRETA_COMPRAR4) {
					SwapCartas(Mao[STVC(j)], Mao[STVC(j + 1)]);
					swap = true;
					continue;
				}
				//Se a carta da esquerda for ACAO COLORIDA, troca, exceto se a direita for +4
				else if (Mao[STVC(j)].GetTipo() == Carta::Tipo::TIPO_ACAO_COR && Mao[STVC(j + 1)].GetNumero() != Carta::Numero::N_PRETA_COMPRAR4) {
					SwapCartas(Mao[STVC(j)], Mao[STVC(j + 1)]);
					swap = true;
					continue;
				}
				//Se a carta da esquerda for Coringa, e a da direita n�o for nem acao colorida, nem +4, trocar
				else if ((Mao[STVC(j)].GetNumero() == Carta::Numero::N_PRETA_ESCOLHER_COR)
					&& (Mao[STVC(j + 1)].GetTipo() != Carta::Tipo::TIPO_ACAO_COR)
					&& (Mao[STVC(j + 1)].GetNumero() != Carta::Numero::N_PRETA_COMPRAR4)) {
					SwapCartas(Mao[STVC(j)], Mao[STVC(j + 1)]);
					swap = true;
					continue;

				}


				//Se ambas forem acao colorida, entramos no terceiro nivel:numeros/simbolos
				else if (static_cast<int>(Mao[STVC(j)].GetTipo()) == static_cast<int>(Carta::Tipo::TIPO_ACAO_COR)
					&& static_cast<int>(Mao[STVC(j + 1)].GetTipo()) == static_cast<int>(Carta::Tipo::TIPO_ACAO_COR)) {

					if (static_cast<int>((Mao[STVC(j)].GetNumero())) > static_cast<int>(Mao[STVC(j + 1)].GetNumero())) {
						SwapCartas(Mao[STVC(j)], Mao[STVC(j + 1)]);
						swap = true;
						continue;
					}

					//Se ambas forem acao colorida e do mesmo simbolo, entramos no segundo nivel:cor
					else if (static_cast<int>(Mao[STVC(j)].GetNumero()) == static_cast<int>(Mao[STVC(j + 1)].GetNumero())) {
						if (Org2(Mao[STVC(j)], Mao[STVC(j + 1)], nCores)) {
							SwapCartas(Mao[STVC(j)], Mao[STVC(j + 1)]);
							swap = true;
							continue;
						}
						else {
							continue;
						}
					}
				}


				//Se ambas forem numericas, vamos direto para o segundo nivel: cor
				else if (Mao[STVC(j)].GetTipo() == Carta::Tipo::TIPO_NUMERICA && Mao[STVC(j + 1)].GetTipo() == Carta::Tipo::TIPO_NUMERICA) {
					if (Org2(Mao[STVC(j)], Mao[STVC(j + 1)], nCores)) {
						SwapCartas(Mao[STVC(j)], Mao[STVC(j + 1)]);
						swap = true;
						continue;
					}
					//Se ambas forem numericas e da mesma cor, vamos para o terceiro nivel: numero/simbolo
					else if (Mao[STVC(j)].GetCor() == Mao[STVC(j + 1)].GetCor()) {
						if (Org3(Mao[STVC(j)], Mao[STVC(j + 1)], nNumeros)) {
							SwapCartas(Mao[STVC(j)], Mao[STVC(j + 1)]);
							swap = true;
							continue;
						}
						else {
							continue;
						}
					}
				}
			}

			if (!swap) {
				break;
			}
		}
	}
}
//terceiro nivel de clasificacao das cartas com base no numero
bool Player::Org3(Carta &a, Carta &b, std::array<std::array<int, 2>, 15> &Numeros) {
	
	if (GetIndexNum(Numeros, a.GetNumero()) > GetIndexNum(Numeros, b.GetNumero())) {
		return true;
	}
	/*else if (GetIndexNum(Numeros, a.GetNumero()) == GetIndexNum(Numeros, b.GetNumero())) {

	}*/
	else {
		return false;
	}
}
// segundo nivel de classificacao das cartas com base na cor
bool Player::Org2(Carta &a, Carta &b, std::array<std::array<int, 2>, 4> &Cores) {
	if (GetIndexCor(Cores, a.GetCor()) > GetIndexCor(Cores, b.GetCor())) {
		return true;
	}
	/*else if (GetIndexCor(Cores, a.GetCor()) == GetIndexCor(Cores, b.GetCor())) {
		return false;
	}*/
	else{
		return false;
	}
}
//Durante a organiza��o da mao do npc (Ver void OrgMaoD()) esta retorna o index de da cor da carta
// dentro de um array organizado pela preval�ncia de cores na mao. 
int Player::GetIndexCor(std::array<std::array < int, 2>, 4> &Cores, Carta::Cor aCor) {
	for (int i{ 0 }; i < 4; i++) {
		if (Cores[STAC(i)][STAC(1)] == aCor)
			return i;
	}
}
// Durante a organiza��o da mao do npc(Ver void OrgMaoD()) esta retorna o index de do numero/simbolo da carta
// dentro de um array organizado pela preval�ncia de numeros/simbolos na mao. 
int Player::GetIndexNum(std::array<std::array < int, 2>, 15> &Numeros, Carta::Numero oNum) {
	for (int i{ 0 }; i < 4; i++) {
		if (Numeros[STAN(i)][STAN(1)] == oNum)
			return i;
	}
}

void Player::SwapCartas(Carta &a, Carta &b) {
	Carta temp = a;
	a = b;
	b = temp;
}
void Player::SwapInt(int &a, int &b) {
	int temp = a;
	a = b;
	b = temp;
}
Carta Player::AI(std::vector<Player> &Jogadores,int &ProximoJog, Carta &ultima, Carta::Cor Cor, bool &jogou, bool &comprou, Carta *proxima, int contParaComprar, bool &BGBotGame) {
	
	bool testarProb{ false };
	if (!BGBotGame){
		if (ultima.GetCor() == Carta::Cor::COR_PRETA) {
			std::cout << "\n   A cor escolhida foi ";
			if (Cor == Carta::Cor::COR_AZUL)
				std::cout << "Azul.\n";
			else if (Cor == Carta::Cor::COR_VERMELHO)
				std::cout << "Vermelho.\n";
			else if (Cor == Carta::Cor::COR_VERDE)
				std::cout << "Verde.\n";
			else if (Cor == Carta::Cor::COR_AMARELO)
				std::cout << "Amarelo.\n";
		}

		if (contParaComprar > 1)
			std::cout << "   Cartas acumuladas para comprar: " << contParaComprar << ". \n";
		else if (contParaComprar == 1 && ((ultima.GetNumero() == Carta::Numero::N_COMPRAR2) || (ultima.GetNumero() == Carta::Numero::N_PRETA_COMPRAR4)))
			std::cout << "   Cartas acumuladas para comprar: 0. Carta ja teve seu efeito.\n\n";
		
		int a{ 0 };
		std::cout << "\n   Digite zero para ver a proxima jogada.\n";
		DigiteZero(a,0, 0, "\n   Digite zero para ver a proxima jogada.\n");
	}

	while (true) {
		
		OrgMaoD();
		//IA OFENSIVA
		if(testarProb){	
			//Implementar a checagem de disposi��o de acordo com as cartas que houver na m�o.
			//Para dar certo preciso arrumar o problema com a mudan�a de posi��o das carta ACAO COLORIDA
			//Ou seja, +2, pular, inverter
			//
			//for (int carta{ GetMaoSize() - 1 }; carta >= 0 && 
			//	(Mao[carta].GetTipo() != Carta::Tipo::TIPO_NUMERICA) &&
			//	(Mao[carta].GetTipo() != Carta::Numero::N_PRETA_ESCOLHER_COR); carta--) {
			//	
			//	std::vector<int> CartasIndexes;
			//	//Checa se para algum inimigo h� disposi��o para atacar com +4
			//	if (TemCarta(Carta::Numero::N_PRETA_COMPRAR4, Carta::Cor::COR_PRETA)) {
			//		if (PossoJogar(ultima, Mao[IndexDaCarta(Carta::Numero::N_PRETA_COMPRAR4, Carta::Cor::COR_PRETA)], Cor, contParaComprar)) {
			//			std::vector<int> AtaqueIndexes;
			//			AtaqueIndex(Jogadores, ProximoJog, Carta::Numero::N_PRETA_COMPRAR4, AtaqueIndexes);
			//			std::vector<int> ProbDefVoltar;
			//			std::vector<int> PotDamage{ 0 };
			//			int PotDamageMe;
			//			ProbDefesa(Jogadores, ProximoJog, PotDamage, PotDamageMe, Carta::Numero::N_Pr;
			//			for (int i{ 1 }; i < Jogadores.size() + 1; i++) {
			//				if (AtaqueIndexes[i - 1] > ProbDefVoltar[i] && AtaqueIndexes[i - 1] > ProbDefVoltar[0] &&
			//					PotDamage[i - 1] > PotDamageMe) {
			//					Carta tempo;
			//					tempo = Mao[IndexDaCarta(Carta::Numero::N_PRETA_COMPRAR4, Carta::Cor::COR_PRETA)];
			//					TirarCartaDaMao(i);
			//					jogou = true;
			//					return tempo;
			//				}
			//			}
			//		}
			//	}
			//}

			//A cada checagem abaixo, este vector guarda a posi��o das cartas na mao do jogador
			//Para o caso de ele ter mais de uma (para as cartas coloridas)
			std::vector<int> CartasIndexes;
			//Checa se para algum inimigo h� disposi��o para atacar com +4
			if (TemCarta(Carta::Numero::N_PRETA_COMPRAR4, Carta::Cor::COR_PRETA)) {
				if (PossoJogar(ultima, Mao[IndexDaCarta(Carta::Numero::N_PRETA_COMPRAR4, Carta::Cor::COR_PRETA)], Cor, contParaComprar)) {
					std::vector<int> AtaqueIndexes;
					AtaqueIndex(Jogadores, ProximoJog, Carta::Numero::N_PRETA_COMPRAR4, AtaqueIndexes);
					std::vector<int> ProbDefVoltar;
					std::vector<int> PotDamage;
					int PotDamageMe{ 0 };
					ProbDefesa(Jogadores,ProximoJog,PotDamage,PotDamageMe,Carta::Numero::N_PRETA_COMPRAR4,ProbDefVoltar);
					//Talvez encontrar o maior valor e checar se ele passa no processo? Mas qual maior... AI ou menor ProbDef...
					//Maior AI faz sentido 
					for (int i{ 1 }; i <Jogadores.size()+1;i++) {
						if (AtaqueIndexes[i - 1] > ProbDefVoltar[i] && AtaqueIndexes[i - 1] > ProbDefVoltar[0] &&
							PotDamage[i - 1] > PotDamageMe) {
							Carta tempo;
							tempo = Mao[IndexDaCarta(Carta::Numero::N_PRETA_COMPRAR4, Carta::Cor::COR_PRETA)];
							TirarCartaDaMao(i);
							jogou = true;
							return tempo;
						}
					}
				}
			}
			//Checa se para algum inimigo h� disposi��o para atacar com +2
			if (TemCarta(Carta::Numero::N_COMPRAR2)) {
				CartasIndexes = PassaOsIndexes(Carta::Numero::N_COMPRAR2);
				for (int c{ 0 }; c > CartasIndexes.size(); c++) {
					if (PossoJogar(ultima, Mao[CartasIndexes[c]], Cor, contParaComprar)) {
						std::vector<int> AtaqueIndexes;
						AtaqueIndex(Jogadores, ProximoJog, Carta::Numero::N_COMPRAR2, AtaqueIndexes);
						std::vector<int> ProbDefVoltar;
						std::vector<int> PotDamage;
						int PotDamageMe{ 0 };
						ProbDefesa(Jogadores, ProximoJog, PotDamage, PotDamageMe, Carta::Numero::N_COMPRAR2, ProbDefVoltar);

						for (int i{ 1 }; i </*Players.size()+1*/0; i++) {
							if (AtaqueIndexes[i - 1] > ProbDefVoltar[i] && AtaqueIndexes[i - 1] > ProbDefVoltar[0] &&
								PotDamage[i - 1] > PotDamageMe) {
								Carta tempo;
								/*tempo = Mao[IndexDaCarta(Carta::Numero::N_PRETA_COMPRAR4, Carta::Cor::COR_PRETA)];*/
								TirarCartaDaMao(i);
								jogou = true;
								return tempo;
							}
						}
					}
				}
			}
			//Checa se para algum inimigo h� disposi��o para atacar com pular
			if (TemCarta(Carta::Numero::N_PULAR)) {
				CartasIndexes = PassaOsIndexes(Carta::Numero::N_PULAR);
				for (int c{ 0 }; c > CartasIndexes.size(); c++) {
					if (PossoJogar(ultima, Mao[CartasIndexes[c]], Cor, contParaComprar)) {
						std::vector<int> AtaqueIndexes;
						AtaqueIndex(Jogadores, ProximoJog, Carta::Numero::N_PRETA_COMPRAR4, AtaqueIndexes);
						std::vector<int> ProbDefVoltar;
						ProbDefesa(Jogadores, ProximoJog,Carta::Numero::N_PULAR, ProbDefVoltar);

						for (int i{ 1 }; i </*Players.size()+1*/0; i++) {
							if (AtaqueIndexes[i - 1] > ProbDefVoltar[i] && AtaqueIndexes[i - 1] > ProbDefVoltar[0]) {
								Carta tempo;
								/*tempo = Mao[IndexDaCarta(Carta::Numero::N_PRETA_COMPRAR4, Carta::Cor::COR_PRETA)];*/
								TirarCartaDaMao(i);
								jogou = true;
								return tempo;
							}
						}
					}
				}
			}
			//Checa se para algum inimigo h� disposi��o para atacar com inverter
			if (TemCarta(Carta::Numero::N_INVERTER)) {
				CartasIndexes = PassaOsIndexes(Carta::Numero::N_INVERTER);
				for (int c{ 0 }; c > CartasIndexes.size(); c++) {
					if (PossoJogar(ultima, Mao[CartasIndexes[c]], Cor, contParaComprar)) {
						std::vector<int> AtaqueIndexes;
						AtaqueIndex(Jogadores, ProximoJog, Carta::Numero::N_PRETA_COMPRAR4, AtaqueIndexes);
						std::vector<int> ProbDefVoltar;
						ProbDefesa(Jogadores, ProximoJog, Carta::Numero::N_INVERTER, ProbDefVoltar);

						for (int i{ 1 }; i </*Players.size()+1*/0; i++) {
							if (AtaqueIndexes[i - 1] > ProbDefVoltar[i] && AtaqueIndexes[i - 1] > ProbDefVoltar[0]) {
								Carta tempo;
								/*tempo = Mao[IndexDaCarta(Carta::Numero::N_PRETA_COMPRAR4, Carta::Cor::COR_PRETA)];*/
								TirarCartaDaMao(i);
								jogou = true;
								return tempo;
							}
						}
					}
				}
			}





		}

		//IA DEFENSIVA
	
		/*PrintMao();*/
		if (false) {
		}
		else {
			for (int i{ 0 }; i < GetMaoSize(); i++) {
				if (PossoJogar(ultima, Mao[STVC(i)], Cor, contParaComprar)) {
					Carta tempo;
					tempo = Mao[STVC(i)];
					TirarCartaDaMao(i);
					jogou = true;
					return tempo;
				}
			}
		}

		if (!comprou) {
			ComprarCarta(proxima, contParaComprar);
			comprou = true;
			if (!BGBotGame)
				std::cout << "\n   Cartas compradas: " << contParaComprar<<".\n";

			if (contParaComprar > 1) {
				jogou = false;
				return Carta();
			}
			else {
				continue;
			}
		}
		else if (comprou) {
			jogou = false;
			return Carta();
		}
	}
}

bool Player::TemCarta(Carta::Numero Num, Carta::Cor color) {
	
	for (Carta &e : Mao) {
		if (e.GetNumero() == Num && e.GetCor() == color)
			return true;
	}
	
	return false;
}
std::vector<int> Player::PassaOsIndexes(Carta::Numero Num) {
	if (TemCarta(Num)) {
		std::vector<int> Indexes;
		int sum{ 0 };
		for (int i{ 0 }; i < GetMaoSize(); i++) {
			if (Mao[i].GetNumero() == Num)
				sum++;
		}

		Indexes.resize(sum);
		int iIndex{ 0 };
		
		for (int i{ 0 }; i < GetMaoSize(); i++) {
			if (Mao[i].GetNumero() == Num) {
				Indexes[iIndex] = i;
				iIndex++;
			}
		}
		return Indexes;
	}

	else 
		return std::vector<int>{-1};
	
}
bool Player::TemCarta(Carta::Cor color) {

	for (Carta &e : Mao) {
		if (e.GetCor() == color)
			return true;
	}

	return false;
}
bool Player::TemCarta(Carta::Numero Num) {

	for (Carta &e : Mao) {
		if (e.GetNumero() == Num)
			return true;
	}

	return false;
}

int Player::IndexDaCarta(Carta::Numero Num, Carta::Cor color) {
	for (int i{ 0 }; i < GetMaoSize();i++) {
		if (Mao[i].GetNumero() == Num && Mao[i].GetCor() == color)
			return  static_cast<std::vector<Carta>::size_type>(i);
	}
}

void Player::DigiteZero(int &number, int min, int max, 
	std::string ErrorMessageOutOfRange, 
	std::string ErrorMessageFail) {
	std::cin.clear();
	do {
		std::cin >> number;
		if (std::cin.fail()) {
			std::cin.clear();
			std::cin.ignore(32676, '\n');
			std::cout << ErrorMessageFail;
			continue;
		}
		else if (number >max || number <min)
			std::cout << ErrorMessageOutOfRange;
		else
			break;
	} while (true);
	
}

void Player::ProbDefesa( std::vector<Player> &Players, const int &ProximoJog,
	const  Carta::Numero Num, std::vector<int> &ProbDefVoltar) {
	
}
Prob Player::GetProb(std::vector<std::vector<Prob>*> &ProbPlayer, std::vector<Player> &Players,  Prob::Ter posse) {
	if (posse == Prob::Ter::TER_N_PRETA_COMPRAR4)
		posse = Prob::Ter::TER_N_COMPRAR2;

	int PIndex{ 0 };
	(*ProbPlayer[PIndex]).resize(8>Players[PIndex].GetMaoSize()? Players[PIndex].GetMaoSize()+(4> Players[PIndex].GetMaoSize()?(Players[PIndex].GetMaoSize()+1):5):(8+5));

	for (int jog{ 0 }; jog < Players.size(); jog++) {
		
		if (jog == 0) {
			int limite{};
			for (int i{ 0 }; 
				i - Players[PIndex].GetMaoSize() <= (Players[PIndex].GetMaoSize()) &&
				i - Players[PIndex].GetMaoSize() <= 4
				; i++) {
				if (i == 0) {
					(*ProbPlayer[PIndex])[i] = Prob(posse, i, Players[PIndex].GetMaoSize(), tamanhoDoDeck);
					Prob MaisQ(Prob::Ter::TER_N_PRETA_COMPRAR4, 0, Players[PIndex].GetMaoSize(), tamanhoDoDeck);
					(*ProbPlayer[PIndex])[i] = ((*ProbPlayer[PIndex])[i])*MaisQ;
				}
				else if (i<= Players[PIndex].GetMaoSize()) {
					(*ProbPlayer[PIndex])[i] = Prob(posse, i, Players[PIndex].GetMaoSize(), tamanhoDoDeck);
				}
				else {
					(*ProbPlayer[PIndex])[i] = Prob(posse, i, Players[PIndex].GetMaoSize(), tamanhoDoDeck);
					Prob MaisQ(Prob::Ter::TER_N_PRETA_COMPRAR4, i - Players[PIndex].GetMaoSize(), Players[PIndex].GetMaoSize(), tamanhoDoDeck);
					(*ProbPlayer[PIndex])[i] = ((*ProbPlayer[PIndex])[i])*MaisQ;						
				}
				
			}
		}

		

		else {
			
			for (int i{ 0 };
				i - Players[PIndex].GetMaoSize() <= (Players[PIndex].GetMaoSize()) &&
				i - Players[PIndex].GetMaoSize() <= 4
				; i++) {
				if (i == 0) {
					(*ProbPlayer[PIndex])[i] = Prob(posse, i, Players[PIndex].GetMaoSize(), tamanhoDoDeck);
					Prob MaisQ(Prob::Ter::TER_N_PRETA_COMPRAR4, 0, Players[PIndex].GetMaoSize(), tamanhoDoDeck);
					(*ProbPlayer[PIndex])[i] = ((*ProbPlayer[PIndex])[i])*MaisQ;
				}
				else if (i <= Players[PIndex].GetMaoSize()) {
					(*ProbPlayer[PIndex])[i] = Prob(posse, i, Players[PIndex].GetMaoSize(), tamanhoDoDeck);
				}
				else {
					(*ProbPlayer[PIndex])[i] = Prob(posse, i, Players[PIndex].GetMaoSize(), tamanhoDoDeck);
					Prob MaisQ(Prob::Ter::TER_N_PRETA_COMPRAR4, i - Players[PIndex].GetMaoSize(), Players[PIndex].GetMaoSize(), tamanhoDoDeck);
					(*ProbPlayer[PIndex])[i] = ((*ProbPlayer[PIndex])[i])*MaisQ;
				}

			}
		}
		
		for (int i{ 0 }; i - Players[PIndex].GetMaoSize() <= (Players[PIndex].GetMaoSize()) &&
			i - Players[PIndex].GetMaoSize() <= 4; i++) {
			if (i < Players[PIndex].GetMaoSize()) {
				std::cout << "\nProb de ter " << i << "+2: ";
				(*ProbPlayer[PIndex])[i].GetFracao().PrintPorcen();
			}
			else {
				std::cout << "\nProb de ter 0 +2 e " << i - Players[PIndex].GetMaoSize() << "+4: ";
				(*ProbPlayer[PIndex])[i].GetFracao().PrintPorcen();
			}
		}
	}

	return Prob();
}
void Player::ProbDefesa( std::vector<Player> &Players, const int &ProximoJog,
	std::vector<int> &PotDamage, int &PotDamageMe,
	const  Carta::Numero Num, std::vector<int> &ProbDefVoltar) {

	ProbDefVoltar.resize(static_cast<std::vector<int>::size_type>(Players.size()));
	PotDamage.resize(static_cast<std::vector<int>::size_type>(Players.size()));
	std::vector<std::vector<Prob>*> ProbPlayer;


	ProbPlayer.resize(Players.size());
	GetProb(ProbPlayer, Players, NumToTer(Num));
	
}
void Player::AtaqueIndex(std::vector<Player> &Players,int &ProximoJog, Carta::Numero Num, std::vector<int> &AtaqueIndexes) {
	;
}

Prob::Ter Player::NumToTer(const Carta::Numero &a) {
	switch (a)
	{
	case Carta::Numero::N_COMPRAR2:
		return Prob::Ter::TER_N_COMPRAR2;

	case Carta::Numero::N_INVERTER:
		return Prob::Ter::TER_N_INVERTER;

	case Carta::Numero::N_PRETA_COMPRAR4:
		return Prob::Ter::TER_N_PRETA_COMPRAR4;

	case Carta::Numero::N_PRETA_ESCOLHER_COR:
		return Prob::Ter::TER_N_PRETA_ESCOLHER_COR;

	case Carta::Numero::N_PULAR:
		return Prob::Ter::TER_N_PULAR;

	default:
		return Prob::Ter::TER_NULO;
	}
}

