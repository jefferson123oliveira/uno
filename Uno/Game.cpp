#include "pch.h"
#include "Game.h"
#include "Carta.h"
#include "Player.h"
#include <vector>
#include <array>
#include <random>
#include <ctime>
#include <iostream>
#include <string>
#include <chrono>
#include <cassert>
#define NDEBUG



extern std::mt19937 mersenne;
extern std::uniform_int_distribution<> EmbDeck;

Game::Game(int numeroPlayers, int numeroDeCartas, Carta nula, int human, std::vector<Carta> TesteMao,
	bool BGBotGame, int NumJogos, bool AcabarNoPrim, bool TempoPassado, bool TestarMao)
	: m_tamanhoDoDeck(tamanhoDoDeck),
	m_numeroPlayers{ numeroPlayers },
	m_numeroDeCartas{ numeroDeCartas },
	m_proxima{ 0 },
	m_direcao{ -1 },
	m_ultima{ nula },
	m_deck{ nula },
	m_descarte(),
	Players(),
	m_humanPlayers(human),
	m_contParaComprar(1),
	m_BGBotGame(BGBotGame),
	m_NumJogos(NumJogos),
	m_AcabarNoPrim(AcabarNoPrim),
	m_TempoPassado(TempoPassado),
	m_TestarMao(TestarMao)
{
	
	Players.resize(static_cast<std::vector<Player>::size_type>(m_numeroPlayers));
	
	for (int i{ 0 }; i < m_numeroPlayers; i++) {
		Players[static_cast<std::vector<Player>::size_type>(i)].SetMaoSize(m_numeroDeCartas);
		Players[static_cast<std::vector<Player>::size_type>(i)].SetID(i);
	}
	for (int i{ 0 }; i < m_humanPlayers; i++) {
		Players[(i)].SetHuman(true);
		std::cout << "\n   Digite o nome do jogador " << i + 1 << ": \n";
		std::string temp;
		std::cin >> temp;
		Players[(i)].SetName(temp);
	}

	
	Simbolos();
	SetSimbolos();
	int cartaIndex{0};
	
	if (!m_BGBotGame)
		std::cout << "Tirando o baralho da caixa...";

	//Estes Loops atribui a cada elemento do array Deck uma carta 
	//Cartas numericas e de acao com cor(96)
	for (int i{ 0 }; i < (m_tamanhoDoDeck / 108); i++) {
		for (int cont{ 0 }; cont < 2; cont++) {
			for (int n{ 1 }; n < Carta::Numero::N_PRETA_COMPRAR4; n++) {
				for (int c{ 0 }; c < Carta::Cor::COR_PRETA; c++, cartaIndex++) {
					int t{ 0 };

					//Cartas de a��o com cor
					if (n > 9) {
						t = 1;
						Game::m_deck[cartaIndex].SetCarta(static_cast<Carta::Tipo>(t), static_cast<Carta::Numero>(n));
						Game::m_deck[cartaIndex].SetCor(static_cast<Carta::Cor>(c));

					}
					//cartas numericas
					else {
						Game::m_deck[cartaIndex].SetCarta(static_cast<Carta::Tipo>(t), static_cast<Carta::Numero>(n));
						Game::m_deck[cartaIndex].SetCor(static_cast<Carta::Cor>(c));
					}

				}
			}
		}
		//Cartas de a��o pretas (8)
		for (int n{ 13 }; n < Carta::Numero::N_MAX; n++) {
			for (int c{ 0 }; c < Carta::Cor::COR_PRETA; c++, cartaIndex++) {
				Game::m_deck[cartaIndex].SetCarta(Carta::Tipo::TIPO_ACAO, static_cast<Carta::Numero>(n));
				Game::m_deck[cartaIndex].SetCor(Carta::Cor::COR_PRETA);
			}

		}
		// Zeros (4 cartas)
		for (int c{ 0 }; c < Carta::Cor::COR_PRETA; c++, cartaIndex++) {
			Game::m_deck[cartaIndex].SetCarta(Carta::Tipo::TIPO_NUMERICA, Carta::Numero::N_0);
			Game::m_deck[cartaIndex].SetCor(static_cast<Carta::Cor>(c));
		}
	}
	
	if (m_TestarMao)
		SetMaoTeste(TesteMao);
}

Game::~Game()
{



}

std::array<std::string, 22>& Game::GetSimbolos() { return m_simbolos; }
std::string& Game::GetSimbolo(int index) { return m_simbolos[index]; }
void Game::Simbolos() {
	int i{ 0 };
	GetSimbolos()[i] = "["; //0
	i++;
	GetSimbolos()[i] = "]"; 
	i++;
	GetSimbolos()[i] = "{"; //1
	i++;
	GetSimbolos()[i] = "}";
	i++;
	GetSimbolos()[i] = "/"; //2 
	i++;
	GetSimbolos()[i] = "\\";
	i++;
	GetSimbolos()[i] = "!"; //3 
	i++;
	GetSimbolos()[i] = "!";
	i++;
	GetSimbolos()[i] = "("; //4
	i++;
	GetSimbolos()[i] = ")";
	i++;
	GetSimbolos()[i] = "-"; //5
	i++;
	GetSimbolos()[i] = "-";
	i++;
	GetSimbolos()[i] = "="; //6
	i++;
	GetSimbolos()[i] = "=";
	i++;
	GetSimbolos()[i] = "^";
	i++;
	GetSimbolos()[i] = "^";
	i++;
	GetSimbolos()[i] = ":";
	i++;
	GetSimbolos()[i] = ":";
	i++;
	GetSimbolos()[i] = "&";
	i++;
	GetSimbolos()[i] = "&";
	i++;
	GetSimbolos()[i] = "�";
	i++;
	GetSimbolos()[i] = "�";

}
void Game::SetMaoTeste(std::vector<Carta> &TesteMao) {
	
	int size = TesteMao.size();
	for (int i{ 0 }; i < size; i++) {
		for (int deck{ 0 }; deck < m_tamanhoDoDeck; deck++) {
			if (m_deck[deck].GetNumero()==TesteMao[i].GetNumero() && m_deck[deck].GetCor() == TesteMao[i].GetCor()) {
				GetPlayers()[0].SetCarta(TirarCarta(deck), i);
				break;
			}
		}
	}
	Embaralhar();
	NulasProFim();
	m_proxima = m_proxima + m_numeroDeCartas;
	
}

void Game::SwapCartas(Carta &a, Carta &b){
	Carta temp = a;
	a = b;
	b = temp;
}

void Game::Embaralhar() {
	
	if(!m_BGBotGame)
		std::cout << "   Embaralhando as cartas...";
	
	for (Carta &e : m_deck) {
			SwapCartas(e, m_deck[static_cast<std::array<Carta, 108>::size_type>(EmbDeck(mersenne))]);
	}
	NulasProFim();
}
//Tira uma carta espec�fica do deck. Precisa ser utilziada junto com
//Um sort que coloque as cartas nulas que ficam para o fim, do contr�rio
Carta Game::TirarCarta(int index) {
	Carta carta = m_deck[STAC(index)];
	m_deck[STAC(index)].SetNula();
	return carta;
}
//Tira cartas compradas do deck e lida com m_contParaComprar


void Game::NulasProFim() {
	for (int i{ 0 }; i < m_tamanhoDoDeck; i++) {
		int menor{ i };
		for (int j{ m_tamanhoDoDeck-1 }; j>i; j--) {
			if (m_deck[menor].GetTipo() == Carta::Tipo::TIPO_NULO)
				break;
			if (m_deck[j].GetTipo() == Carta::Tipo::TIPO_NULO && m_deck[menor].GetTipo() != Carta::Tipo::TIPO_NULO)
				menor = j;
		}
		SwapCartas(m_deck[i], m_deck[menor]);
	}
	/*PrintDeck();*/
}
Carta Game::TirarCarta(bool &comprou) {
		
	Carta carta;
	if (comprou) {
		
		for (int i{ 0 }; i < m_contParaComprar; i++) {
			carta = m_deck[STAC(m_proxima)];
			m_deck[STAC(m_proxima)].SetNula();
			m_proxima++;
		}
		m_contParaComprar = 1;
	}
	
	//if (((static_cast<int>(m_deck.size()) - STVC(m_proxima)) < (m_contParaComprar) )|| m_proxima>101) {
	//	//Reembaralha o descarte
	//	if(m_BGBotGame)
	//		std::cout << "\n   Reembaralhando descarte...\n\n";

	//	std::cout<<"\n !!RE!! " <<m_descarte.size();

	//		for (int i{ 0 }; (STAC(i) < m_descarte.size() - STAC(2)); i++) {
	//		m_deck[i] = m_descarte[i];
	//	}

	//	/*PrintDeck();*/
	//	Embaralhar();
	//	/*PrintDeck();*/
	//	//Coloca as cartas nulas pro come�o
	//	NulasProFim();
	//	/*for (int i{ 0 }; i < 2; i++) {
	//		std::cout << "des" << i << ".";
	//		m_descarte[i].PrintCarta();
	//	}*/

	//	m_proxima = m_tamanhoDoDeck + 1 - ((m_tamanhoDoDeck - m_proxima) + (static_cast<int>(m_descarte.size()) - 2) + 1);
	//	std::cout << "p" << m_proxima << " ";
	//	if (static_cast<int>(m_descarte.size()) == (0))
	//		std::cout << " TIRAR ANTES ";
	//	m_descarte[STVC(1)] = m_descarte[m_descarte.size() - STVC(1)];
	//	if (static_cast<int>(m_descarte.size()) == (0))
	//		std::cout << " TIRAR ULTIMA";
	//	m_descarte[STVC(0)] = m_descarte[m_descarte.size() - STVC(2)];
	//	if (static_cast<int>(m_descarte.size()) == (0))
	//		std::cout << " TIRAR PENULTIMA";
	//	m_descarte.resize(STVC(2));
	//	if (static_cast<int>(m_descarte.size()) == (0))
	//		std::cout << " TIRAR RESIZE";
	//	assert(m_deck[m_proxima].GetTipo() != Carta::Tipo::TIPO_NULO
	//		&& "Apos reembaralhar, a proxima carta no deck e NULA.");
	//	
	//	return carta;
	//}
	//

	return carta;
}

void Game::Reemb() {

	if (((static_cast<int>(m_deck.size()) - STVC(m_proxima)) < (m_contParaComprar)) || m_proxima > 101) {
		//Reembaralha o descarte
		if (!m_BGBotGame)
			std::cout << "\n   Reembaralhando descarte...\n\n";

		for (int i{ 0 }; (STAC(i) < m_descarte.size() - STAC(2)); i++) {
			m_deck[i] = m_descarte[i];
		}

		
		Embaralhar();
		
		//Coloca as cartas nulas pro come�o
		NulasProFim();

		m_proxima = m_tamanhoDoDeck + 1 - ((m_tamanhoDoDeck - m_proxima) + (static_cast<int>(m_descarte.size()) - 2) + 1);
		if (static_cast<int>(m_descarte.size()) == (0))
			std::cout << " TIRAR ANTES ";
		m_descarte[STVC(1)] = m_descarte[m_descarte.size() - STVC(1)];
		if (static_cast<int>(m_descarte.size()) == (0))
			std::cout << " TIRAR ULTIMA";
		m_descarte[STVC(0)] = m_descarte[m_descarte.size() - STVC(2)];
		if (static_cast<int>(m_descarte.size()) == (0))
			std::cout << " TIRAR PENULTIMA";
		m_descarte.resize(STVC(2));
		if (static_cast<int>(m_descarte.size()) == (0))
			std::cout << " TIRAR RESIZE";
		assert(m_deck[m_proxima].GetTipo() != Carta::Tipo::TIPO_NULO
			&& "Apos reembaralhar, a proxima carta no deck e NULA.");

	}

}

void Game::DistribuirCartas() {
	int player{ 0 };
	if (m_TestarMao)
		player++;
	bool yes{ true };
	for (; player < m_numeroPlayers; player++) {
		for (int maoIndex{ 0 }; maoIndex < m_numeroDeCartas; maoIndex++) {
			Players[static_cast<std::vector<Player>::size_type>(player)].SetCarta(TirarCarta(yes), maoIndex);
		}
	}
	bool temp{ true };
	DescartarCarta(TirarCarta(temp), true);
}

void Game::PrintDeck() {
	int carta{ 0 };
	for (Carta &e : m_deck) {
		if (carta<10)
			std::cout << " ";
		if (carta < 100)
			std::cout << " ";
		
		std::cout << carta<<" - ";
		e.PrintCarta();
		if (carta == m_proxima)
			std::cout << " <<proxima<<";
		carta++;
	}
	
	std::cout << "\n\n\n";
}

int* Game::GetNumeroDeCartas() { return &m_numeroDeCartas; }
bool Game::CheckMaos(const int &JogAnterior) {

	
		if (GetPlayers()[STVP(JogAnterior)].GetMaoSize() == 1 && !m_BGBotGame) {
			if (GetPlayers()[STVP(JogAnterior)].GetHuman())
				std::cout << "\n   " << GetPlayers()[STVP(JogAnterior)].GetName() << " esta de UNO.";
			else
				std::cout << "\n   O jogador " << JogAnterior + 1 << " esta de UNO!\n";
		}
	
	for (int i{ 0 }; i < m_numeroPlayers; i++) {
		if (GetPlayers()[STVP(JogAnterior)].GetMaoSize() == 0)
				return true;	
	}
	return false;
}
void Game::PrintJogRodada(int prox, int j, int espaco) {
	for (int e{ 0 }; e < espaco; e++) {
		std::cout << " ";
	}
	if(prox==j && GetPlayers()[STVC(j)].GetHuman())
		std::cout << ">" << GetPlayers()[STVC(j)].GetSimbolo() << GetPlayers()[STVC(j)].GetMaoSize() << GetPlayers()[STVC(j)].GetSimbolo2() << "<";
	else if (prox == j && !GetPlayers()[STVC(j)].GetHuman())
		std::cout << ">" << GetPlayers()[STVC(j)].GetSimbolo() << GetPlayers()[STVC(j)].GetMaoSize() << GetPlayers()[STVC(j)].GetSimbolo2() << "<";
	else if (prox != j && GetPlayers()[STVC(j)].GetHuman())
		std::cout << " " << GetPlayers()[STVC(j)].GetSimbolo() << GetPlayers()[STVC(j)].GetMaoSize() << GetPlayers()[STVC(j)].GetSimbolo2() << " ";
	else if (prox != j && !GetPlayers()[STVC(j)].GetHuman())
		std::cout << " " << GetPlayers()[STVC(j)].GetSimbolo() << GetPlayers()[STVC(j)].GetMaoSize() << GetPlayers()[STVC(j)].GetSimbolo2() << " ";

	for (int e{ 0 }; e < espaco; e++) {
		std::cout << " ";
	}
}

//Printa a rodada: o numero de cartas na mao de cada jogador e 
// A ultima carta jogada
void Game::PrintRodada(int ProximoJog) {
	//if(GetPlayers()[STVP(ProximoJog)].GetHuman())
	//std::cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"; //30 linhas

	
		std::cout << "\n\n\n\n\n\n\n\n\n\n"; //10 linhas

	int espacosBase{((100/((m_numeroPlayers/2)+1))/2)};
	
	for (int j{ 0 }; j < (m_numeroPlayers/2); j++) {
		PrintJogRodada(ProximoJog, j, espacosBase);
	}
	std::cout << "\n\n\n";
	
	for (int i{ 0 }; i < espacosBase*m_numeroPlayers/2; i++) {
		std::cout << " ";
	}
	GetUltima().PrintCarta();
	std::cout << "\n\n\n";
	espacosBase = ((100 / ((m_numeroPlayers / 2) + 1)) / 2);
	for (int j{ (m_numeroPlayers-1)}; j > ((m_numeroPlayers / 2) - 1); j--) {
		PrintJogRodada(ProximoJog, j, espacosBase);
	}
	std::cout << "\n\n";
}
//Pega a carta passada e coloca no vector de descarte, aplica os efeitos da carta jogada. 
// Problema com o +4 
void Game::DescartarCarta(Carta carta, bool jogou) {

	if (!jogou) {
		/*m_contParaComprar = 1;*/
		if (m_direcao == 2) {
			m_direcao = 1;
		}
		if (m_direcao == -2) {
			m_direcao = -1;
		}
		return;
	}
	else {
		//PEga a carta recebida e coloca no vector de descarte
		m_descarte.resize(m_descarte.size() + STVC(1));
		m_descarte[(m_descarte.size() - STVC(1))] = carta;
				
		//Seta efeitos de cartas e desfaz efeitos anteriores
		if (((carta.GetNumero() == Carta::Numero::N_INVERTER ))){
			if (m_direcao == 2)
				m_direcao = 1;

			else if (m_direcao == -2)
				m_direcao = -1;

			m_direcao *= -1;
			
			return;
		}
		else if (carta.GetNumero() == Carta::Numero::N_PULAR && (m_direcao == 2 ||m_direcao== -2)) {
			return;
		}
		else if ((carta.GetNumero() == Carta::Numero::N_PULAR)) {
			m_direcao *= 2;
			return;
		}
		else if (carta.GetNumero() == Carta::Numero::N_COMPRAR2) {
			if (m_contParaComprar == 1) {
				m_contParaComprar = 2;
				
			}
			else {
				m_contParaComprar += 2;
				
			}

		}
		else if (carta.GetNumero() == Carta::Numero::N_PRETA_COMPRAR4) {
			if (m_contParaComprar == 1) {
				m_contParaComprar = 4;
			}
			else if (m_contParaComprar>1) {
				m_contParaComprar = m_contParaComprar + 4;
			}

		}
		
		if (m_direcao == 2) {
			m_direcao = 1;
			return;
		}
		else if (m_direcao == -2) {
			m_direcao = -1;
			return;
		}
	}

}

Carta& Game::GetUltima() {
	return m_descarte[m_descarte.size()-static_cast<std::vector<Carta>::size_type>(1)];
}
Carta& Game::GetPenulti() {
	return (m_descarte[m_descarte.size() - static_cast<std::vector<Carta>::size_type>(2)]);
}

std::vector<int> Game::Play() {

	//Lidam com a contagem de vit�rias
	std::vector<int> Vitorias;
	Vitorias.resize(m_numeroPlayers);
	int countVit{ 0 };
	int countJogada{ 0 };

	bool jogoutemp{ false };
	int JogAnterior{ m_numeroPlayers-1 };
	int ProximoJog{ 0 }; //Guarda o index do proximo jogador
	
	if ((GetUltima().GetNumero() == Carta::Numero::N_PULAR || GetUltima().GetNumero() == Carta::Numero::N_INVERTER) && m_numeroPlayers == 2) {
		ProximoJog = 1;
	}
	else if (ProximoJog == 0 && m_direcao == 1)
		ProximoJog += m_direcao;
	else if (m_direcao == -2)
		ProximoJog = m_numeroPlayers - 1;

	bool escolheu{ false };//checa se uma carta coringa j� teve seu efeito 
	
	Carta::Cor ultimaCor(GetUltima().GetCor());
	
	if(ultimaCor==Carta::Cor::COR_PRETA)
		ultimaCor= Players[STVC(ProximoJog)].EscolherCor(GetUltima(), GetUltima(), ultimaCor, escolheu, jogoutemp);
	
	while (true) {
		 
		while (true) {
			
			
			if (!m_BGBotGame)
				PrintRodada(ProximoJog);

			if (Players[STVP(ProximoJog)].GetHuman()) {
				
				std::cout << "\n   "  << "  "<< GetPlayers()[ProximoJog].GetName()
					<< ", digite zero para jogar: \n";
				int zero;
				GetPlayers()[ProximoJog].DigiteZero(zero, 0, 0, "\n   Digite zero para jogar: \n");
				
			}
			else {
				
				
			}

			
			bool jogou{ true };
			bool comprou{ false };
			Carta jogada = Players[STVP(ProximoJog)].Jogar(GetPlayers(),ProximoJog, GetUltima(), ultimaCor, jogou, comprou, &m_deck[STAC(m_proxima)], m_contParaComprar, m_BGBotGame);
			
			TirarCarta(comprou);
			DescartarCarta(jogada, jogou);
			Reemb();
				
			////Se a carta for preta deixa o jogador escolher a cor;
			ultimaCor = (Players[STVC(ProximoJog)].EscolherCor(GetUltima(), GetPenulti(), ultimaCor, escolheu, jogou));
			countJogada++;
			//Define o proximo jogador com base na prosicao do jogador atual dentro do vector
			// e com base na direcao (hor�rio, anti hor�rio) e se h� pulo ou nao
			{
				JogAnterior = ProximoJog;
				if (!((jogada.GetNumero() == Carta::Numero::N_PULAR || jogada.GetNumero() == Carta::Numero::N_INVERTER) && m_numeroPlayers == 2)) {
					if (ProximoJog == (m_numeroPlayers - 1) && (m_direcao == 1 || m_direcao == 2))
						ProximoJog = 0 + m_direcao - 1;
					else if (ProximoJog == (m_numeroPlayers - 2) && m_direcao == 2)
						ProximoJog = 0;
					else if (ProximoJog == 0 && (m_direcao == -1 || m_direcao == -2))
						ProximoJog = m_numeroPlayers + m_direcao;
					else if (ProximoJog == 1 && m_direcao == -2)
						ProximoJog = m_numeroPlayers - 1;
					else
						ProximoJog = ProximoJog + m_direcao;
				}
			}
			
			//Checa se algu�m tem 0 ou 1 carta na m�o.
			if (CheckMaos(JogAnterior)) {
				break;
			}
			//Se o jog anterior era humano, sobe a jogada dele para que outros jogadores n�o vejam
			if(GetPlayers()[JogAnterior].GetHuman() && !m_BGBotGame)
			std::cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
		}

		//Roda quando alguem ganha. Printa quem ganhou
		//sendo humano ou  o computador
		{
			if (GetPlayers()[STVP(JogAnterior)].GetHuman()) {
				std::cout << "\n\n   " << GetPlayers()[STVP(JogAnterior)].GetName() << " ganhou! Parabens!\n\n";
			}
			else {
				if (!m_BGBotGame)
					std::cout << "\n\n O computador ganhou.\n\n";
			}
		}
		//Printa a rodada mostrando a carta jogada pelo vencedor
		//e seu numero zero de cartas
		if (!m_BGBotGame)
			PrintRodada(JogAnterior);
		//Se o numero de players era igual a 2, o vector de vit�rias
		//� atualizado e o o jogo acaba.
		if (m_numeroPlayers == 2) {
			if (!m_BGBotGame)
				std::cout << " O jogo acabou!";
			
			if (GetPlayers()[ProximoJog].GetHuman())
				std::cout << " " << GetPlayers()[ProximoJog].GetName() << ", boa sorte da proxima vez!\n";
			
			if (!m_AcabarNoPrim) {
				Vitorias[countVit] = GetPlayers()[JogAnterior].GetID();
				countVit++;
			}
			else {
				if (JogAnterior == 0)
					Vitorias[0] = 1;
				else
					Vitorias[0] = 0;
			}
			break;
		}
		//Se o numero � maior que dois, o jogo pergunta se o jogador quer continuar
		//Se
		if (!m_AcabarNoPrim) {
			Vitorias[countVit] = GetPlayers()[JogAnterior].GetID();
			countVit++;

			if (!m_BGBotGame)
				std::cout << "   Se quiser continuar jogando, com os jogadores que sobraram, digite 0.\n   Se quiser come�ar outro jogo, ou fechar, digite 1.";
			
			int zero{ 1 };
			if (!m_BGBotGame) {
				do {
					std::cin.clear();
					std::cin >> zero;
					std::cin.ignore(32767, '\n');
				} while (zero != 0 && zero != 1);
			}
			else
				zero = 0;

			if (zero == 0) {
				if (ProximoJog > JogAnterior)
					ProximoJog -= 1;

				m_numeroPlayers -= 1;

				int size(static_cast<int>(GetPlayers().size()));
				std::vector<Player>::size_type ST(static_cast<int>(GetPlayers().size()));

				for (int i{ JogAnterior }; i < (size - 1); i++) {
					GetPlayers()[STVP(i)] = GetPlayers()[STVP(i + 1)];
				}

				Players.resize(ST - STVP(1));

				if (!m_BGBotGame)
					std::cout << "\n   Continuando o jogo com " << m_numeroPlayers << " jogadores.";
				
				continue;
			}
		}
		else {
			if (JogAnterior == 0)
				Vitorias[0] = 1;
			else 
				Vitorias[0] = 0;
			break;
		}
	}
	return Vitorias;
}

std::vector<Player>& Game::GetPlayers() {
	return Players;
}

void Game::SetSimbolos() {
	int simb{ 0 };
	for (int j{ 0 }; j < m_numeroPlayers; j++) {
		if (GetPlayers()[j].GetHuman()) {
			GetPlayers()[j].SetSimbolo(GetSimbolo(simb), GetSimbolo(simb + 1));
			simb += 2;
		}
	}
}

int Game::GetContParaComprar() { return m_contParaComprar; }
std::vector<Carta>::size_type  Game::STVC(int a) {
	return static_cast<std::vector<Carta>::size_type>(a);
}

std::vector<Player>::size_type Game::STVP(int a) {
	return static_cast<std::vector<Player>::size_type>(a);
}
std::array<Carta, 108>::size_type Game::STAC(int a) {
	return static_cast<std::array<Carta, 108>::size_type>(a);
}
