/////////////////////////////////////////////////////////////////
// 
// Uno.cpp : Esse arquivo tem a fun��o main, aqui inicia e termina a execu��o do programa. 
// Ele tbm contem configura��es e fun��es para setar elas. 
// Algumas dessas configura��es s�o passadas para uma inst�ncia de Game
// Outras tem a ver com teste de vari�veis fixas. 
//
/////////////////////////////////////////////////////////////////

#include "pch.h"
#include "Prob.h"
#include <iostream>
#include "Carta.h"
#include "Player.h"
#include "Game.h"
#include <random>
#include <ctime>
#include <vector>
#include <array>
#include <string>
#include"Fracao.h"

std::mt19937 mersenne(static_cast<unsigned int>(std::time(nullptr)));
//Distribui��o usada para definir o n�mero de embaralhadas para melhorar a aleatoriedade das cartas
std::uniform_int_distribution<> NdeEmb(1, 3);
//Distribui��o usada dentro de Game para embaralhar as cartas 
std::uniform_int_distribution<> EmbDeck(0, (108 - 1));

//C:\SFML\include geberal c++
// linker C:\SFML\lib
// sfml-main-d.lib;sfml-graphics-d.lib;sfml-system-d.lib;sfml-window-d.lib;sfml-network-d.lib;sfml-audio-d.lib;
//kernel32.lib;user32.lib;gdi32.lib;winspool.lib;comdlg32.lib;advapi32.lib;shell32.lib;ole32.lib;oleaut32.lib;uuid.lib;odbc32.lib;odbccp32.lib;%(AdditionalDependencies)

/////////////////////////////////////////////////////////////////
//
// Fun��o usada para pedir um n�mero do usu�rio. O n�mero � 
// restringido pelos int min e max e os erros de entrada s�o 
// lidados apropriadamente.
//
/////////////////////////////////////////////////////////////////
int IntInput(const int &min, const int &max, const int &padrao) {
	int a{ padrao };
	do {
		std::cin >> a;

		if (std::cin.fail()) {
			std::cin.clear();
			std::cin.ignore(32676, '\n');
			std::cout << "\n   Tente novamente: \n";
		}
		else if (a > max)
			std::cout << "\n   Numero muito grande: \n";
		else if (a < min)
			std::cout << "\n   Numero muito pequeno: \n";
		else
			break;
		} while (true);
	return a;
}
/////////////////////////////////////////////////////////////////
//
//	Fun��o usada por ConfigTestarMao() para receber o input 
//	do n�mero da carta a ser testada
//
/////////////////////////////////////////////////////////////////
void SetNumCarta(Carta &carta) {
	do {
		std::string Numero;
		std::cin >> Numero;
		
		if (Numero == "0") {
			carta.SetNumero(Carta::Numero::N_0);
			carta.SetTipo(Carta::Tipo::TIPO_NUMERICA);
		}

		else if (Numero == "1"){
			carta.SetNumero(Carta::Numero::N_1);
			carta.SetTipo(Carta::Tipo::TIPO_NUMERICA);
		}
		else if (Numero == "2"){
			carta.SetNumero(Carta::Numero::N_2);
			carta.SetTipo(Carta::Tipo::TIPO_NUMERICA);
		}
		else if (Numero == "3"){
			carta.SetNumero(Carta::Numero::N_3);
			carta.SetTipo(Carta::Tipo::TIPO_NUMERICA);
		}
		else if (Numero == "4"){
			carta.SetNumero(Carta::Numero::N_4);
			carta.SetTipo(Carta::Tipo::TIPO_NUMERICA);
		}
		else if (Numero == "5"){
			carta.SetNumero(Carta::Numero::N_5);
			carta.SetTipo(Carta::Tipo::TIPO_NUMERICA);
		}
		else if (Numero == "6"){
			carta.SetNumero(Carta::Numero::N_6);
			carta.SetTipo(Carta::Tipo::TIPO_NUMERICA);
		}
		else if (Numero == "7"){
			carta.SetNumero(Carta::Numero::N_7);
			carta.SetTipo(Carta::Tipo::TIPO_NUMERICA);
		}
		else if (Numero == "8"){
			carta.SetNumero(Carta::Numero::N_8);
			carta.SetTipo(Carta::Tipo::TIPO_NUMERICA);
		}
		else if (Numero == "9"){
			carta.SetNumero(Carta::Numero::N_9);
			carta.SetTipo(Carta::Tipo::TIPO_NUMERICA);
		}
		else if (Numero == "p" || Numero == "P"){
			carta.SetNumero(Carta::Numero::N_PULAR);
			carta.SetTipo(Carta::Tipo::TIPO_ACAO_COR);
		}
		else if (Numero == "i" || Numero == "I"){
			carta.SetNumero(Carta::Numero::N_INVERTER);
			carta.SetTipo(Carta::Tipo::TIPO_ACAO_COR);
		}
		else if (Numero == "c2" || Numero == "C2"){
			carta.SetNumero(Carta::Numero::N_COMPRAR2);
			carta.SetTipo(Carta::Tipo::TIPO_ACAO_COR);
		}
		else if (Numero == "c4" || Numero == "C4"){
			carta.SetNumero(Carta::Numero::N_PRETA_COMPRAR4);
			carta.SetTipo(Carta::Tipo::TIPO_ACAO);
			carta.SetCor(Carta::Cor::COR_PRETA);
		}
		else if (Numero == "cor" || Numero == "Cor" || Numero == "COR"){
			carta.SetNumero(Carta::Numero::N_PRETA_ESCOLHER_COR);
			carta.SetTipo(Carta::Tipo::TIPO_ACAO);
			carta.SetCor(Carta::Cor::COR_PRETA);
		}

	} while (carta.GetNumero()==Carta::Numero::N_NULO);
}

/////////////////////////////////////////////////////////////////
//
// Fun��o usada para especificar as cartas da m�o a ser testada
// em um n�mero de jogos dado pelo usu�rio. � chamada de 
// Background bot game e Testar M�o estiverem ativos
//
/////////////////////////////////////////////////////////////////
void ConfigTestarMao(std::vector<Carta> &TesteMao) {
	int MaoSize(10);
	std::cout << "\n   Digite o tamanho da mao:\n";
	MaoSize = IntInput(1,15,MaoSize);
	if (MaoSize == 1)
		return;
	TesteMao.resize(MaoSize);
	
	for (int i{ 0 }; i < MaoSize; i++) {
		std::cout << "\n   Digite a carta n#" << i + 1 << " desta forma: \n";
		std::cout << "   Primeiro o numero ou simbolo. Utilize de 0 a 9, ou, \n    p      PULAR\n"
			<< "   i      INVERTER\n   c2      COMPRAR 2\n   c4      COMPRAR 4\n   cor      ESCOLHER COR/CORINGA\n";
		SetNumCarta(TesteMao[i]);
		
		if (TesteMao[i].GetTipo() == Carta::TIPO_NUMERICA || TesteMao[i].GetTipo() == Carta::TIPO_ACAO_COR) {
			std::cout << "   Agora escolha a cor da carta. \n   1 - AZUL \n   2 - VERMELHO \n   3 - VERDE \n   4 - AMARELO\n";
			int cor{ 1 };
			cor = IntInput(1, 4, cor);
			TesteMao[i].SetCor(static_cast<Carta::Cor>((cor - 1)));
		}
	}

}
/////////////////////////////////////////////////////////////////
//
// Esta fun��o muda o estado de um bool para o oposto. Usada 
// ao lidar com as configura��es do jogo.
//
/////////////////////////////////////////////////////////////////
void ChangeState(bool &config) {
	if (config == true)
		config = false;
	else
		config = true;

}
/////////////////////////////////////////////////////////////////
//
// Esta fun��o roda se o jogador entrar 142 ao inv�s de 0 no
// in�cio. Aqui ele pode mudar as configura��es.
//
/////////////////////////////////////////////////////////////////
void Config(bool &BGBotGame, int &NumJogos, bool &TestarMao, bool &AcabarNoPrim, bool &TempoPassado, std::vector<Carta> &TesteMao) {
	//Para defini��es das configura��es, olhar main
	std::string SBGBotGame("Background Bot Game ");
	std::string SNumJogos("Numero de jogos para rodar ");
	std::string STestarMao("Testar uma mao ");
	std::string SAcabarNoPrimeiro("Terminar o jogo no primeiro vencedor ");
	std::string STempoPassado ("Checar tempo passado ");
	std::string SPrintProb("Printar probabilidades ");

	int config{ 0 };
	//O usu�rio muda os valores das configura��es at� digite 0
	do {
	std::cout << "\n Configuracoes: \n\n   1 " << SBGBotGame <<" " << ((BGBotGame == 1) ? ("ON") : ("OFF"))
		<< " \n      2 "<<SNumJogos  << NumJogos
		<< " \n      3 " << STestarMao << ((TestarMao == 1) ? ("ON") : ("OFF"))
		<< " \n   4 " << SAcabarNoPrimeiro << ((AcabarNoPrim == 1) ? ("ON") : ("OFF"))
		<< " \n   5 "<<STempoPassado << ((TempoPassado == 1) ? ("ON") : ("OFF"));
	
	
		std::cout<<"\n\n Digite o numero correspondente da Configuracao para muda-la, ou digite 0 para sair: \n";
		config = IntInput(0,5,config);
		
		if (config == 1)
			ChangeState(BGBotGame);

		else if (config == 4)
			ChangeState(AcabarNoPrim);

		else if (config == 5)
			ChangeState(TempoPassado);

		else if (config == 2 && BGBotGame) {
			std::cout << "\n   Digite o numero de jogos que deseja que rode no modo BackGround Bot Game:\n ";
			NumJogos = IntInput(1, 500000, NumJogos);
		}
		else if (config == 2 && !BGBotGame) 
			std::cout << "\n   Ative o modo BackGround Bot Game primeiro:\n ";
		else if (config == 3 && BGBotGame)
			ChangeState(TestarMao);
		else if (config == 3 && !BGBotGame)
			std::cout << "\n   Ative o modo BackGround Bot Game primeiro:\n ";


	} while(config!=0);
	//Caso TestarMao estiver ON, o usu�rio � chamado a definir as cartas 
	if (TestarMao == 1)
		ConfigTestarMao(TesteMao);
}

int main()
{
	//Descarta os primeiros valores dessas distribui��es. 
	NdeEmb(mersenne);
	EmbDeck(mersenne);
	/////////////////////////////////////////////////////////////////
	//Este timer � usado para calcular o tempo de para cada jogo
	//caso a config TempoPassado estiver ON
	/////////////////////////////////////////////////////////////////
	Timer t;
	/////////////////////////////////////////////////////////////////
	//Caso esta confi estiver ON o programa roda o n�mero NumJogos  
	//de jogos testando uma vari�vel fixa. No momento a �nica vari�vel  
	//fixa poss�vel � uma m�o (TestarMao) especificada pelo usu�rio. 
	// No futuro vou usar esta config para testar diferentes 
	// ofensividades da intelig�ncia artificial.
	/////////////////////////////////////////////////////////////////
	bool BGBotGame{ false };
	//Ver coment�rio acima.
	int NumJogos{ 1 };
	//Ver coment�rio acima.
	bool TestarMao{ false };
	// Essa config termina o jogo no primeiro vencedor, sem perguntar 
	// ao jogador se ele quer continuar.
	bool AcabarNoPrim{ false };
	//Ver coment�rio em Timer t; acima
	bool TempoPassado{ false };
	// Este vector guarda as cartas definidas pelo jogador para o teste.
	//Ver coment�rio em BGBotGame acima
	std::vector<Carta> TesteMao;
	/////////////////////////////////////////////////////////////////
	//Este vector guarda os dados sobre as vit�rias do jogo
	//Caso AcabarNoPrim estiver ON ele vai guardar somente 1 ou 0.
	// 1 em caso de o jogador de interesse tiver ganhado, 0 em caso 
	// de ele ter perdido. No momento o jogador de interesse � apenas 
	// o 0, o primeiro, pois � sempre ele que leva a Mao fixa para teste.
	// ver BGBotGame acima
	/////////////////////////////////////////////////////////////////
	std::vector<std::vector<int>> Vitorias;
	std::cout << "\n\n   ==================================================================================\n   =\n";
	
	std::cout << "   =   Simbolos usados no jogo: \n   =    @ - pular o proximo jogardor \n   =    <> - inverter a direcao do jogo \n   =    * - coringa/escolher cor \n"
		<<"   =   Os numeros na tela mostram a quantidade de cartas na mao de cada jogador.\n   =   Os > < indicam o jogador da rodada, por exemplo:"
		<<" >7<. \n   =   Os outros simbolos em torno dos numeros ([], {}, !!) indicam cada jogador humano. \n   =";
	std::cout << "==================================================================================\n\n\n";
	
	while (true) {
		std::cout << "   Digite zero para iniciar ou 1 para sair: ";

		int a{ 0 };
		//Numeros das vari�veis para iniciar o jogo 
		int nCartas{ 10 }; 
		//Numeros das vari�veis para iniciar o jogo 
		int nJog{ 3 };
		//Numeros das vari�veis para iniciar o jogo 
		int nHuman{ 0 };

		do {
			std::cin >> a;

			if (std::cin.fail()) {
				std::cin.clear();
				std::cin.ignore(32676, '\n');
				std::cout << "\n   Tente novamente: \n";
			}
			else if (a != 0 && a!=142 && a!=1)
				std::cout << "\n   Digite zero para iniciar: \n";
			else
				break;
		} while (true);


		if (a == 142) {
			Config(BGBotGame, NumJogos, AcabarNoPrim, TempoPassado, TestarMao, TesteMao);
		}
		if (a == 1)
			break;
		//countGames guarda o n�mero de jogos jogados. Utilizado
		// em conjunto com BGBotGame. Ver coment�rio acima
		int countGames{ 0 };
		//Estes ifs setam o vector Vit�ria de acordo com as config 
		if (BGBotGame)
			Vitorias.resize(NumJogos);
		else
			Vitorias.resize(1);
		if (countGames == 0 && AcabarNoPrim && BGBotGame) {
			for (int i{ 0 }; i < NumJogos; i++)
				Vitorias[i].resize(1);
		}

		Timer T;
		//Este while � um jogo, desde a defini��o das vari�veis (cartas, jogadores)
		// at� o momento de passar os dados de vit�rias para o vector Vit�rias
		while (true) {

			////////////////////////////////////////////////////////////
			//Esta parte de defini��es s� roda se BGBotGame estiver OFF
			// ou, se estiver ON, ele roda apenas no primeiro jogo, assim,
			//nos pr�ximos o jogo inicia com as mesmas vari�veis
			/////////////////////////////////////////////////////////////
			if (!BGBotGame || countGames == 0) {
				if (!TestarMao) {
					std::cout << "   Defina o numero de cartas por jogador: \n";

					do {
						if (nCartas > 15)
							std::cout << "\n   Essa quantidade e muito grande: \n";
						if (nCartas < 5 && nCartas>0)
							std::cout << "\n   Essa quantidade e muito pequena: \n";
						if (nCartas == 0)
							std::cout << "\n   Nao ha como jogar com zero cartas: \n";
						if (nCartas < 0)
							std::cout << "\n   Essa quantidade e menor do que zero: \n";

						nCartas = IntInput(5, 15, nCartas);
						
					} while (std::cin.fail() || nCartas > 15 || nCartas < 5);
				}

				else {
					nCartas = static_cast<int>(TesteMao.size());
					std::cout << "\n   Numero inicial de cartas por jogador setado para " << TesteMao.size() <<".\n";
				}
				std::cout << "   Defina o numero de jogadores totais (humanos e bots): \n";

				do {
					std::cin >> nJog;

					if (std::cin.fail()) {
						std::cin.clear();
						std::cin.ignore(32676, '\n');
						std::cout << "\n   Tente novamente: \n";
					}

					else if (nJog < 2)
						std::cout << "\n   Nao ha como jogar com menos do que 2 jogadores: \n";
					else if (nJog > 15 && nCartas*nJog < 96)
						std::cout << "\n   Essa quantidade de jogadores e muito grande: \n";
					else if (nCartas*nJog > 96) {
						std::cout << "\n   Com esse numero de jogadores e esse numero de cartas sobraria muito poucas cartas no deck.\n"
							<< "   Tente um numero menor: \n";
					}
					else
						break;
				} while (true);

				if (!BGBotGame) {
					std::cout << "   Defina o numero de jogadores humanos: \n";
					do {
						std::cin >> nHuman;

						if (std::cin.fail()) {
							std::cin.clear();
							std::cin.ignore(32676, '\n');
							std::cout << "\n   Tente novamente: \n";
						}
						else if (nHuman < 0) {
							std::cout << "\n   Esse numero e menor do que zero.\n"
								<< "   Se quiser nenhum jogador humano, digite zero:   \n";
						}
						else if (nHuman > nJog)
							std::cout << "\n   O numero de jogadores humanos tem que ser menor ou igual ao numero de jogadores totais, definido anteriormente: \n";
						else
							break;
					} while (true);
				}
			
				if (!AcabarNoPrim && BGBotGame) {
					for (int i{ 0 }; i < NumJogos; i++)
						Vitorias[i].resize(nJog);
				}
				else if (!BGBotGame) {
					Vitorias[0].resize(nJog);
				}
			}

			if (countGames == 0 && TempoPassado)
				T.Reset();
			//Cria uma inst�ncia de game 
			Game *game= new Game(nJog, nCartas, Carta(), nHuman, TesteMao, BGBotGame, NumJogos, AcabarNoPrim, TempoPassado, TestarMao);

		
			
			int emb = NdeEmb(mersenne);
			//Embaralha as cartas do deck
			for (int count{ 0 }; count < emb; count++) {
				game->Embaralhar();
			}
		
			game->DistribuirCartas();
			
			//A depender da config AcabarNoPrim o vector Vitorias
			//ter� um tamanho diferente, assim como o vector retornado por Play ser� diferente. 
			if(AcabarNoPrim)
			Vitorias[countGames][0] =game->Play()[0];
			
			else
				Vitorias[countGames] = game->Play();

			delete game;
			
			if (!BGBotGame) {
				std::cout << "\n   Se quiser iniciar um novo jogo, digite 0, se quiser fechar, digite 1: \n";
				int zero{ 0 };
				zero = IntInput(0, 1, zero);

				if (zero == 0) {
					continue;
				}
				else
					break;
			}

			//Caso BGBotGame==ON definimos se � necess�rio rodar um novo jogo 
			//ou se os jogos j� acabaram. Neste caso, informamos o n�mero de jogos rodados.
			else {

				if (countGames+1 < NumJogos) {
					countGames++;
					continue;
				}
				else {
					//Preciso checar se n�o h� um erro by one neste countGames
					std::cout << countGames+1 << ((countGames > 0) ? (" jogos rodados.") : (" jogo rodado."));
					break;
				}
			}
		}
		
		if (TempoPassado)
			std::cout << "\n   Tempo passado: " << T.TempoPassado() << " segundos.\n\n";
		//Caso TestarMao estiver ON, calcula em quantos jogos a mao teste venceu.
		if (TestarMao && AcabarNoPrim) {
			/*for (int count{ 0 }; count < NumJogos; count++) {
				std::cout<< (Vitorias[static_cast<std::vector<std::vector<int>>::size_type>(count)][static_cast<std::vector<int>::size_type>(0)]);
				std::cout << "  ";
				if (count % 10 == 9)
					std::cout << "\n";
			}*/

			double sum{ 0 };
			for (int i{ 0 }; i < NumJogos; i++) {
				sum=sum+static_cast<double>(Vitorias[static_cast<std::vector<std::vector<int>>::size_type>(i)][static_cast<std::vector<int>::size_type>(0)]);
			}

			std::cout << "\n   Sua mao teste ganhou de primeiro em " << (sum / (countGames+1)) * 100 << "% dos "<< countGames+1 << " jogos.\n";
			
		}


	}
	return 1;
}
