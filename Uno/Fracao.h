#pragma once
#include <vector>
class Fracao
{
	std::vector<int> m_nume;
	int m_numeLength{1};
	std::vector<int> m_deno;
	int m_denoLength{1};
	bool m_numeFat;
	bool m_denoFat;

public:
	Fracao();
	Fracao(int numerador, int denominador);
	Fracao(std::vector<int> &numerador,  std::vector<int> &denominador);
	~Fracao();
	void AddNume(int a);
	void AddDeno(int a);
	friend Fracao operator/(const Fracao &a, const Fracao &b);
	friend Fracao operator-( Fracao &a,  Fracao &b);
	friend Fracao operator*(const Fracao &a, const Fracao &b);
	friend Fracao operator+( Fracao &a,  Fracao &b);
	void SetFracao(std::vector<int> &numerador, std::vector<int> &denominador);
	void SetFracao(int numerador, int denominador);
	void Simplificar();
	void FatorarNume();
	void FatorarDeno();
	void FatoreVector(std::vector<int> &Vector, int &Length);
	void SomeEstes(std::vector<int> &a, std::vector<int> &b, std::vector<int> &c);
	void FatoreEste(std::vector<int> &Numeros,const int &index, int &Length);
	std::vector<int>::size_type STVI(int a) { return static_cast<std::vector<int>::size_type>(a); }
	void Swap(int &a, int &b);
	double PrintValor();
	void PrintPorcen();
	void PrintFracao();

};

