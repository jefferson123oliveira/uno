#include "pch.h"
#include "Prob.h"
#include "Fracao.h"
#include <iostream>
#include <cassert>
/////////////////////////////////////////////////////////////////
//
//Prob � uma classe usada para facilitar o calculo das probabilidades
//de jogadores terem cartas com as quais eles podem se defender de
//ataques. 
//
/////////////////////////////////////////////////////////////////

//#include "Game.h"

extern const int tamanhoDoDeck;
int Prob::QuantNoBaralho(Ter ter) {
	int mult{ tamanhoDoDeck/108 };

	switch (ter) {
	case TER_UMA_COR:
		return mult*25;
	case TER_DUAS_CORES:
		return mult * 50;
	case TER_TRES_CORES:
		return mult * 75;
	case TER_QUATRO_CORES:
		return mult * 100;
	case TER_N_PULAR:
		return mult * 8;
	case TER_N_INVERTER:
		return mult * 8;
	case TER_N_COMPRAR2:
		return mult * 8;
	case TER_N_PRETA_COMPRAR4:
		return mult * 4;
	case TER_N_PRETA_ESCOLHER_COR:
		return mult * 4;
	case TER_NULO:
		return 0;
	}
}
int Prob::QuantNoBaralho(NaoTer nTer) {
	return (tamanhoDoDeck-QuantNoBaralho(static_cast<Ter>(nTer)));
}
//Qualquer quantidade
Prob::Prob(Ter ter, int MaoSize, int baralho){
	Fracao Univ(1, 1);
	Prob((Univ - Prob(static_cast<NaoTer>(ter), MaoSize, baralho).GetFracao()), ter);
}
//Iniciando uma Prob alocando diretamente a Fra��o resultante
Prob::Prob(Fracao a, Ter ter) {
	m_Probabilidade = a;
	m_ter = ter;
}
Prob::Prob(NaoTer nTer, int MaoSize, int baralho){
	std::vector<int> PNumerador;
	std::vector<int> PDenominador;
	PNumerador.resize(MaoSize);
	PDenominador.resize(MaoSize);
	{
		int limInferior{baralho};
		limInferior = limInferior - QuantNoBaralho(nTer) - MaoSize;
		for (int i{( baralho - QuantNoBaralho(nTer) )}, count{ 0 }; i > limInferior; i--, count++) {
			PNumerador[count] = i;
		}
	}

	{
		int limiteInferior{ baralho - MaoSize };
		for (int i{baralho}, count{ 0 }; i > limiteInferior; count++, i--) {
			PDenominador[count] = i;
		}
	}

	m_Probabilidade.SetFracao(PNumerador, PDenominador);
	m_Probabilidade.PrintFracao();
	m_Probabilidade.Simplificar();
}
Prob::Prob(Ter ter, int quantNaMao, int MaoSize, int baralho){
	Prob(baralho, MaoSize, QuantNoBaralho(ter), quantNaMao);
}

Prob::Prob(Ter ter, int quantNaMao, int MaoSize, int baralho, int SomaMSoutros, int SomaQNMoutros) {
	assert(baralho-SomaMSoutros>0);
	Prob(baralho-SomaMSoutros, MaoSize, (QuantNoBaralho(ter)-SomaMSoutros), quantNaMao);
}

Prob::Prob()
{
}

/////////////////////////////////////////////////////////////////
//
//No caso espec�fico da probabilidade de um jogador ter tantas 
//cartas, os argumentos da fun��o querem dizer o seguinte:
//		x: numero de cartas no baralho
//		y: numero de cartas na mao do jogador
//		a: quantidade da carta em quest�o no baralho
//		   numero total dessas cartas no baralho
//		b: numero da carta em quest�o que estaria na m�o do jogador
//		L�-se ent�o: Probabilidade do jogador ter b cartas de interesse 
//		numa m�o de y cartas, sendo que o baralho tem x cartas no total
//		e a cartas de interesse.
/////////////////////////////////////////////////////////////////

Prob::Prob(const int &x, const int &y, const int &a, const int &b) {

	std::vector<int> numePerm;
	std::vector<int> denoPerm;
	denoPerm.resize(b);
	numePerm.resize(b);

	std::vector<int> numeProb;
	std::vector<int> denoProb;
	denoProb.resize(y-b);
	numeProb.resize(y - b);

	std::vector<int> numeUni;
	std::vector<int> denoUni;
	denoUni.resize(y);
	numeUni.resize(y);

	{
		for (int i{ 0 }; i < y; i++) {
			denoUni[i] = y - i;
		}

		for (int i{ 0 }; i < y; i++) {
			numeUni[i] = x - i;
		}
	}

	{
		for (int i{ 0 }; i < b; i++) {
			denoPerm[i] = b - i;
		}

		for (int i{ 0 }; i < b; i++) {
			numePerm[i] = a - i;
		}
	}

	{
		for (int i{ 0 }; i < (y-b); i++) {
			denoProb[i] = (y-b) - i;
		}

		for (int i{ 0 }; i < (y-b); i++) {
			numeProb[i] = (x-a) - i;
		}
	}

	Fracao Uni(numeUni, denoUni);
	
	Fracao Proba(numeProb, denoProb);

	Fracao Perm(numePerm, denoPerm);
	
	GetFracao() = Proba / Uni;
	
	GetFracao() = GetFracao() * Perm;
	
}

Prob operator/(Prob &a, int &b) {
	Prob c;
	c = a;
	c.m_Probabilidade.AddDeno(b);
	return c;
}
void Prob::InputProb() {
	std::cout << "\n   Entre os digitos de C(x,y), primeiro x: \n";
	int x;
	std::cin >> x;
	int y;
	std::cout << "\n   Entre os digitos de C(x,y),\n Agora y: \n";
	std::cin >> y;

	(*this) = Prob(x, y, 1, 1);
}

Prob Prob::ReturnMe() {
	return *this;
}
Prob::~Prob()
{
}
void Prob::TurnToTh() {
	int Nume = (GetFracao().PrintValor() * 10000);
	GetFracao().SetFracao(Nume, 10000);
}
//Preciso atualizar os tipos ter e nao ter nas prob para vectors,
//Pra conseguir indicar que a prob � com rela��o a mais de um tipo de 
//evento - uma intersec��o.
Prob operator/(Prob &a, Prob &b) {
	Prob c;
	c.GetFracao() = (a.GetFracao())/(b.GetFracao());
	return c;
}
Prob operator-(Prob &a, Prob &b) {
	Prob c;
	a.TurnToTh();
	b.TurnToTh();
	c.GetFracao() = (a.GetFracao())-(b.GetFracao());
	return c;
}
Prob operator*(Prob &a,  Prob &b) {
	Prob c;
	c.GetFracao()= (a.GetFracao())*(b.GetFracao());
	return c;
}
Prob operator+(Prob &a, Prob &b) {
	Prob c;
	a.TurnToTh();
	b.TurnToTh();
	c.GetFracao() = (a.GetFracao())+(b.GetFracao());
	
	return c;
}

